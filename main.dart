import 'package:flutter/material.dart';
import 'package:myapp/loginandregistration.dart';
import 'package:myapp/dashboard.dart';
import 'package:myapp/first_screen.dart';
import 'package:myapp/second_screen.dart';
import 'package:myapp/userprofile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My App',
      home: Login(),
    );
  }
}
